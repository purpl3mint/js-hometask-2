# js_homework_2_scope

## Ответьте на вопросы

* Что такое область видимости? Какие области видимости вы знаете?
> Ответ:
* В какой момент создаётся лексическое окружение и что в него входит?
> Ответ:
* Чем функциональная область видимости отличается от блочной?
> Ответ:
* Где будет видны переменные, объявленные с помощью var, let и const?
> Ответ:
* Изучите механизм замыканий, в каких случаях в JavaScript используют замыкания?
> Ответ:

## Выполните задания

* Установи зависимости npm `install`;
* Допиши функции в `tasks.js`;
* Проверяй себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.

## Требования к заданиям:

* 6 и 7 Задачи "под звездочкой", их решать не обзательно, но будет круто, если сделаешь. Для выполнения нужно для их расомментировать тесты в файле `tasks.test.js`. Перед выполенением рекомендуем прочитать [документацию](https://developer.mozilla.org/ru/docs/Web/API/WindowTimers/setTimeout).
* проходятся тесты;
* осмысленные названия переменных и функций;
* использование const, let если необходимо;
* допускаем использование break только в конструкции switch-case;
* отсутствие невыполнимого кода (после return в функциях или в условиях, которые никогда не выполнятся);
* правильное использование return в функциях (корректно возвращать только одно значение).
